const loginData = require('../loginData');

const botData = {
  page: null,
  browser: null,
  botMessage: 'Cześć! Tutaj BOT ROMAN, Jakuba w tej chwili nie ma, jednak powiadomiłem go o twojej wiadomości. Jak wróci postara się jak najszybciej Tobie odpowiedzieć',
  email: loginData.email,
  password: loginData.password,
  port: 5000,
};
module.exports = botData;
