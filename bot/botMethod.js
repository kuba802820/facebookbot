const botData = require('./config/botData');

const actionOfBot = {
  focusOnDOM: async (selector) => {
    try {
      const { page } = botData;
      await page.focus(selector);
    } catch (error) {
      throw new Error(error);
    }
  },
  goToWEB: async (link) => {
    try {
      const { page } = botData;
      await page.goto(link, { timeout: 0, waitUntil: 'load' });
    } catch (error) {
      throw new Error(error);
    }
  },
  clickSelector: async (selector) => {
    try {
      const { page } = botData;
      await page.click(selector);
    } catch (error) {
      throw new Error(error);
    }
  },
  typeText: async (text, time) => {
    try {
      const { page } = botData;
      await page.keyboard.type(text, { delay: time });
    } catch (error) {
      throw new Error(error);
    }
  },
  getDOMValue: async (selector) => {
    try {
      const { page } = botData;
      await page.waitFor(selector);
      const value = await page.$eval(selector, el => el.innerText);
      return value;
    } catch (error) {
      throw new Error(error);
    }
  },
};

module.exports = actionOfBot;
