/* eslint-disable no-await-in-loop */
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const actionOfBot = require('./botMethod');
const botData = require('./config/botData');

const bot = {
  init: async () => {
    let { browser } = botData;
    browser = await puppeteer.launch({
      defaultViewport: null,
      headless: false,
    });
    botData.page = await browser.newPage();
    await actionOfBot.goToWEB('https://www.facebook.com/login.php');
  },
  getMessageCount: async () => {
    const { getDOMValue } = actionOfBot;
    const msgCount = await getDOMValue('#mercurymessagesCountValue');
    return msgCount;
  },
  authorize: async (email, password) => {
    const { focusOnDOM, clickSelector, typeText} = actionOfBot;
    const { page } = botData;
    console.log('Trwa Logowanie..');
    await page.waitFor('#email');
    await focusOnDOM('#email');
    await typeText(email);
    await page.waitFor('#pass');
    await focusOnDOM('#pass');
    await typeText(password);
    await clickSelector('#loginbutton');
    await actionOfBot.goToWEB('https://www.facebook.com/messages/t/100018182832724');
    console.log('zalogowano');
  },
  getUnreadMessage: async () => {
    const { page } = botData;
    const convers = [];
    try {
      const bodyHTML = await page.evaluateHandle(() => document.querySelector('[aria-label="Conversation List"]').innerHTML);
      const html = bodyHTML._remoteObject.value;
      const $ = cheerio.load(html);
      await $('li').each((i, v) => {
        if (v.attribs.class.search('_1ht3') !== -1) {
          convers[i] = { html: $(v).html() };
        }
      });
      return convers;
    } catch (error) {
      throw new Error(error);
    }
  },
  execMessage: async (conversationUnread) => {
    for (let i = 0; i < conversationUnread.length; i++) {
      const { page } = botData;
      await actionOfBot.goToWEB(conversationUnread[i].link);
      console.log(`Przejście do: ${conversationUnread[i].name}`);
      await page.click('._1mj');
      console.log('Kliknięcie textboxa');
      await page.waitFor(500);
      console.log('Wpisywanie wiadomości');
      await actionOfBot.typeText(botData.botMessage, 5);
      console.log('Wysyłanie wiadomosci');
      await page.keyboard.press('Enter');
      console.log('Odklikiwanie kursora');
      await page.waitFor('#bluebarRoot');
      await page.click('#bluebarRoot');
      console.log('Przejście na nieużywaną konwersacje');
      await actionOfBot.goToWEB('https://www.facebook.com/messages/t/100018182832724');
      return conversationUnread[i].id;
    }
  },
};
module.exports = bot;
