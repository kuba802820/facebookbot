// const http = require('http');

// const server = http.createServer();
// const io = require('socket.io')(server);

const cheerio = require('cheerio');
const bot = require('./bot/bot');
const botData = require('./bot/config/botData');


let isSending = false;
const taskObject = [];
const listenInterval = 10000;
const IngoreGroupsID = [
  'row_header_id_thread:1929120460501011',
  'row_header_id_thread:1989846404433728',
  'row_header_id_thread:1057400201030820',
  'row_header_id_thread:1458057134278422',
  'row_header_id_thread:1538099912979355',
  'row_header_id_thread:1982293705149728',
  'row_header_id_thread:1963373350423264',
  'row_header_id_thread:2723605617654447',
];

const execMessage = async () => {
  isSending = true;
  const id = await bot.execMessage(taskObject);
  isSending = false;
  const indexID = taskObject.map(x => x.id).indexOf(id);
  if (indexID > -1) {
    taskObject.splice(indexID, 1);
  }
};
const createTask = async (unreadMessage) => {
  for (let i = 0; i < unreadMessage.length; i++) {
    if (unreadMessage[i] !== undefined) {
      const $ = cheerio.load(unreadMessage[i].html);
      const idmsg = $('div').first().attr('id');
      const linkmsg = $('a').first().data('href');
      const username = $('._1qt3').data('tooltip-content');
      const allIDs = taskObject.map(x => x.id);
      if (!allIDs.includes(idmsg) && !IngoreGroupsID.includes(idmsg)) {
        taskObject.push({
          id: idmsg,
          link: linkmsg,
          name: username,
        });
      }
    }
  }
};

const listenMessage = async () => {
  setInterval(async () => {
    if (!isSending) {
      // const actualMsgCount = await bot.getMessageCount();
      const unreadMessage = await bot.getUnreadMessage();
      if (unreadMessage.length > 0) {
        await createTask(unreadMessage);
        await execMessage(taskObject);
      }
    }
  }, listenInterval);
};

// io.on('connection', (socket) => {
//   console.log("Connect: "+socket.id+""+socket.request.connection.remoteAddress);
//   socket.on('Bot_Authorize', async (data) => {
//     if (typeof data === 'boolean') {
//       if(data){
       
//       }
//     }
//   });
// });

const main = async () => {
  await bot.init();
  await bot.authorize(botData.email, botData.password);
  await listenMessage();  
};
main();
//server.listen(botData.port, () => { console.log(`Listening ${botData.port}`); });
